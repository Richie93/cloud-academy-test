export default class RickMorty {

    constructor() {}

    buildPortalLicense(rickMortyObj) {

        let wubbaLubbaDubDub = document.querySelector('.js-wubba-lubba-dub-dub');
        let characterTemplate = `
            <div class="rm-card">
                <div class="card-title-wrapper">
                    PORTAL LICENSE
                </div>
                <div class="card-content-wrapper">
                    <div class="card-content">
                        <img src="${rickMortyObj.image}" class="character-img"
                            alt="">
                    </div>
                    <div class="card-content">
                        <h2 class="character-name">${rickMortyObj.name}</h2>
                        <div class="character-info-wrapper">
                            <div class="character-column">
                                Gender : <strong>${rickMortyObj.gender}</strong>
                            </div>
                            <div class="character-column">
                                Species : <strong>${rickMortyObj.species}</strong>
                            </div>
                            <div class="character-column">
                                Status : <strong>${rickMortyObj.status}</strong>
                            </div>
                            <div class="character-column">
                                Origin : <strong>${rickMortyObj.origin.name}</strong>
                            </div>
                            <div class="character-column">
                                Location : <strong>${rickMortyObj.location.name}</strong>
                            </div>
                            <div class="character-column">
                                Amount of residents : <strong>${rickMortyObj.amountResidents}</strong>
                            </div>
                            <div class="character-column">
                                First seen in : <strong>${rickMortyObj.episode}</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `;

        wubbaLubbaDubDub.insertAdjacentHTML('beforeend', characterTemplate);

    }

    getEpisode(url) {
        let episodeRequest = new XMLHttpRequest();
        return new Promise((resolve, reject) => {
            episodeRequest.open('GET', url, true);
            episodeRequest.setRequestHeader("Content-Type", "application/json");
            episodeRequest.onload = function (event) {
                resolve(JSON.parse(event.target.responseText));
            };
            episodeRequest.send();
        })
    }

    getAmountResidents(url) {
        let amountResidentsRequest = new XMLHttpRequest();
        return new Promise((resolve, reject) => {
            amountResidentsRequest.open('GET', url, true);
            amountResidentsRequest.onload = function (event) {
                resolve(JSON.parse(event.target.responseText));
            };
            amountResidentsRequest.send();
        })
    }

    getRickMortyCharacters(rickMortyApiBaseUrl) {
        let rickMortyRequest = new XMLHttpRequest();
        return new Promise((resolve, reject) => {
            rickMortyRequest.open('GET', rickMortyApiBaseUrl + 'character', true);
            rickMortyRequest.setRequestHeader("Content-Type", "application/json");
            rickMortyRequest.onload = function (event) {
                let rickMortyJson = JSON.parse(event.target.responseText);
                resolve(JSON.parse(event.target.responseText));
            };
            rickMortyRequest.send();
        })
    }

    welcomeToDimensionCOneHundredThirtySeven() {
        let rickMortyApiBaseUrl = 'https://rickandmortyapi.com/api/';
        this.getRickMortyCharacters(rickMortyApiBaseUrl).then(charactersResult => {

            let rickMortyCharacters = charactersResult.results;
            rickMortyCharacters.forEach(async (character) => {
                let urlAmountResidents = character.location.url;
                let firstSeenEpisode = character.episode[0];
                let promiseResult = await Promise.all([this.getAmountResidents(urlAmountResidents), this.getEpisode(firstSeenEpisode)]).then(res => {
                    character.amountResidents = res[0].residents.length;
                    character.episode = res[1].name;
                    this.buildPortalLicense(character);
                }).catch(error => {})

            });
        })
    }

}